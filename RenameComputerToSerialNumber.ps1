﻿$computertype = (Get-WmiObject -Class Win32_ComputerSystem).PCSystemType

# PCSystemType Identifier
#Value	Meaning
#0 (0x0)	Unspecified
#1 (0x1)	Desktop
#2 (0x2)	Mobile
#3 (0x3)	Workstation
#4 (0x4)	Enterprise Server
#5 (0x5)	Small Office and Home Office (SOHO) Server
#6 (0x6)	Appliance PC
#7 (0x7)	Performance Server
#8 (0x8)	Maximum

If ($computertype -eq (1 -or 2 -or 3)){
    Write-Host "This is a DESKTOP, MOBILE or WORKSTATION. Changing hostname..."

    $serial = gwmi win32_bios | select -ExpandProperty SerialNumber
    rename-computer -newname "CC-$serial"

} else {
    Write-Host "This is a server! NOT changing hostname!"
}